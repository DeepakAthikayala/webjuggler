import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeepakComponent } from './deepak/deepak.component';


const routes: Routes = [
  { path: 'deepak', component: DeepakComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
